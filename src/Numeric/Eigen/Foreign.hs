{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE MagicHash           #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UnboxedTuples       #-}

module Numeric.Eigen.Foreign where

import           Control.Exception        (bracket_, finally)
import           Control.Monad            (forM, forM_)
import           Data.Proxy               (Proxy (..))
import qualified Data.Vector              as V
import qualified Data.Vector.Generic      as GV
import qualified Data.Vector.Storable     as SV
import           Foreign                  (free, mallocBytes)
import           Foreign.Marshal.Array    (advancePtr, allocaArray, peekArray)
import           Foreign.Storable         (Storable)
import           GHC.Exts                 (Int (..), Ptr (..))
import           GHC.Prim                 (copyByteArrayToAddr#,
                                           sizeofByteArray#)
import           GHC.Types                (IO (..))
import           Numeric.MPFR.Raw.Safe
import           Numeric.MPFR.Types
import           Numeric.Rounded
import           Numeric.Rounded.Internal (Rounded (..), asByteArray, precBytes)

mapArrayM_ :: Storable a => Int -> (Ptr a -> IO ()) -> Ptr a -> IO ()
mapArrayM_ len f ptr =
  forM_ [0 .. len-1] $ \i -> f (advancePtr ptr i)

withOutRoundedVec
  :: forall r p a . Precision p
  => Int
  -> (Ptr MPFR -> IO a)
  -> IO (V.Vector (Rounded r p), a)
withOutRoundedVec len f =
  allocaArray len $ \mpfr_t_ptr ->
  bracket_
  (mapArrayM_ len (`mpfr_init2` prec) mpfr_t_ptr)
  (mapArrayM_ len mpfr_clear mpfr_t_ptr) $ do
    a <- f mpfr_t_ptr
    mpfrs <- peekArray len mpfr_t_ptr
    xs <- fmap (V.fromListN len) $ forM mpfrs $
      \MPFR{ mpfrPrec = prec', mpfrSign = s, mpfrExp = e, mpfrD = d } ->
      if prec /= prec'
      then error "Tried to marshal MPFR with the wrong precision"
      else asByteArray d (precBytes prec) $ \l ->
      return (Rounded prec s e l)
    return (xs, a)
  where
    prec = fromIntegral (precision (Proxy :: Proxy p))

withInRoundedVec
  :: V.Vector (Rounded r p)
  -> (Ptr MPFR -> IO a)
  -> IO a
withInRoundedVec xs f = do
  mpfrs <- fmap GV.convert (mapM mallocRounded xs)
  SV.unsafeWith mpfrs f
    `finally` SV.mapM_ (free . mpfrD) mpfrs

mallocRounded :: Rounded r p -> IO MPFR
mallocRounded (Rounded prec sgn e ba#) = do
  let bytes = I# (sizeofByteArray# ba#)
  ptr@(Ptr addr#) <- mallocBytes bytes
  IO (\s -> (# copyByteArrayToAddr# ba# 0# addr# (sizeofByteArray# ba#) s, () #))
  return MPFR
    { mpfrPrec = prec
    , mpfrSign = sgn
    , mpfrExp = e
    , mpfrD = ptr
    }
