{-# LANGUAGE DataKinds                #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE PolyKinds                #-}
{-# LANGUAGE ScopedTypeVariables      #-}
{-# LANGUAGE PatternSynonyms          #-}

module Numeric.Eigen.Raw where

import           Control.Exception        (Exception, throw)
import           Control.Arrow            (first)
import qualified Data.Matrix              as M
import           Data.Proxy               (Proxy (..))
import qualified Data.Vector              as V
import           Foreign.C.Types
import           GHC.Exts                 (Ptr)
import           Numeric.Eigen.Foreign
import           Numeric.MPFR.Types
import           Numeric.Rounded
import           System.IO.Unsafe         (unsafePerformIO)

newtype ComputationInfo = ComputationInfo CInt
  deriving (Eq, Ord)

pattern Success        :: ComputationInfo
pattern Success         = ComputationInfo 0
pattern NumericalIssue :: ComputationInfo
pattern NumericalIssue  = ComputationInfo 1
pattern NoConvergence  :: ComputationInfo
pattern NoConvergence   = ComputationInfo 2
pattern InvalidInput   :: ComputationInfo
pattern InvalidInput    = ComputationInfo 3
    
data EigenException
  = NumericalIssueException String
  | NoConvergenceException String
  | InvalidInputException String
  | UnknownException String
  deriving (Eq, Ord, Show)
           
instance Exception EigenException

defaultCheckInfo :: String -> a -> ComputationInfo -> a
defaultCheckInfo functionName a info = case info of
  Success        -> a
  NumericalIssue -> throw (NumericalIssueException functionName)
  NoConvergence  -> throw (NoConvergenceException functionName)
  InvalidInput   -> throw (InvalidInputException functionName)
  _              -> throw (UnknownException functionName)
           
withInRoundedMat
  :: M.Matrix (Rounded r p)
  -> (Ptr MPFR -> IO a)
  -> IO a
withInRoundedMat m = withInRoundedVec (M.getMatrixAsVector m)

withOutRoundedMat
  :: forall r p a . Precision p
  => Int
  -> Int
  -> (Ptr MPFR -> IO a)
  -> IO (M.Matrix (Rounded r p), a)
withOutRoundedMat nRows nCols f =
  first (M.fromList nRows nCols . V.toList) <$>
  withOutRoundedVec (nRows * nCols) f

foreign import ccall "eigen_lu_solve" eigen_lu_solve 
  :: CInt -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> MPFRRnd -> MPFRPrec -> IO ComputationInfo

luSolve
  :: forall r p . (Rounding r, Precision p)
  => M.Matrix (Rounded r p)
  -> V.Vector (Rounded r p)
  -> V.Vector (Rounded r p)
luSolve a b =
  uncurry (defaultCheckInfo "luSolve") $ unsafePerformIO $
  withInRoundedMat a $ \aPtr ->
  withInRoundedVec b $ \bPtr ->
  withOutRoundedVec len $ \xPtr ->
  eigen_lu_solve (fromIntegral len) aPtr bPtr xPtr rnd prec
  where
    len  = length b
    rnd  = fromIntegral (fromEnum (rounding (Proxy :: Proxy r)))
    prec = fromIntegral (precision (Proxy :: Proxy p))

foreign import ccall "eigen_eigensystem_self_adjoint" eigen_eigensystem_self_adjoint
  :: CInt -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> MPFRRnd -> MPFRPrec -> IO ComputationInfo

-- | Returns a vector of eigenvalues of 'm' and a Matrix whose columns
-- are the eigenvectors of 'm'. Here, 'm' is assumed to be symmetric.
eigensystemSymmetric
  :: forall r p . (Rounding r, Precision p)
  => M.Matrix (Rounded r p)
  -> (V.Vector (Rounded r p), M.Matrix (Rounded r p))
eigensystemSymmetric a =
  unpack $ unsafePerformIO $
  withInRoundedMat a $ \aPtr ->
  withOutRoundedVec len $ \evalsPtr ->
  withOutRoundedMat len len $ \evecsPtr ->
  eigen_eigensystem_self_adjoint (fromIntegral len) aPtr evalsPtr evecsPtr rnd prec
  where
    len  = M.nrows a
    rnd  = fromIntegral (fromEnum (rounding (Proxy :: Proxy r)))
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (v, (m, info)) = defaultCheckInfo "eigensystemSymmetric" (v, m) info
      
eigenvaluesSymmetric :: (Rounding r, Precision p) => M.Matrix (Rounded r p) -> V.Vector (Rounded r p)
eigenvaluesSymmetric = fst . eigensystemSymmetric
      
eigenvectorsSymmetric :: (Rounding r, Precision p) => M.Matrix (Rounded r p) -> M.Matrix (Rounded r p)
eigenvectorsSymmetric = snd . eigensystemSymmetric

foreign import ccall "eigen_cholesky_decomposition" eigen_cholesky_decomposition
  :: CInt -> Ptr MPFR -> Ptr MPFR -> MPFRRnd -> MPFRPrec -> IO ComputationInfo

-- | Given a postive-definite matrix A, returns L where A = LL^T and L
-- is lower-triangular.
choleskyDecomposition
  :: forall r p . (Rounding r, Precision p)
  => M.Matrix (Rounded r p)
  -> Maybe (M.Matrix (Rounded r p))
choleskyDecomposition a =
  unsafePerformIO $ do
    (a', info) <- withInRoundedMat a $ \aPtr ->
      withOutRoundedMat (M.nrows a) (M.ncols a) $ \lPtr ->
      eigen_cholesky_decomposition (fromIntegral len) aPtr lPtr rnd prec
    return $ case info of
      NumericalIssue -> Nothing
      _ -> defaultCheckInfo "choleskyDecomposition" (Just a') info
  where
    len = M.nrows a
    rnd  = fromIntegral (fromEnum (rounding (Proxy :: Proxy r)))
    prec = fromIntegral (precision (Proxy :: Proxy p))

      
foreign import ccall "eigen_ldl_decomposition_pivoted" eigen_ldl_decomposition_pivoted
  :: CInt -> Ptr MPFR -> Ptr MPFR -> Ptr MPFR -> MPFRRnd -> MPFRPrec -> IO ComputationInfo
     
-- | Given a matrix A, returns (PTL, D) where PTL = P^T L where P is a
-- permutation matrix and L is lower-triangular with 1's on the
-- diagonal. D is a vector, such that PTL diag(D) PTL^T = A. It is
-- trivial to recover P^T from L by looking for the first 1 when
-- reading down a column. Only gives a well-defined result when acting
-- on positive-definite matrices.
ldlDecompositionPivoted
  :: forall r p . (Rounding r, Precision p)
  => M.Matrix (Rounded r p)
  -> (M.Matrix (Rounded r p), V.Vector (Rounded r p))
ldlDecompositionPivoted a =
  unpack $ unsafePerformIO $
  withInRoundedMat a $ \aPtr ->
  withOutRoundedMat len len $ \ptlPtr ->
  withOutRoundedVec len $ \dPtr ->
  eigen_ldl_decomposition_pivoted (fromIntegral len) aPtr ptlPtr dPtr rnd prec
  where
    len  = M.nrows a
    rnd  = fromIntegral (fromEnum (rounding (Proxy :: Proxy r)))
    prec = fromIntegral (precision (Proxy :: Proxy p))
    unpack (ptl, (d, info)) = defaultCheckInfo "ldlDecompositionPivoted" (ptl, d) info

