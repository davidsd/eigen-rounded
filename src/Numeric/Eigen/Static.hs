{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Numeric.Eigen.Static where

import           Control.Exception  (Exception, throw)
import qualified Data.Matrix        as MU
import           Data.Matrix.Static (Matrix)
import qualified Data.Matrix.Static as M
import           Data.Proxy         (Proxy (..))
import qualified Data.Vector        as V
import           GHC.TypeNats       (KnownNat, natVal)
import           Linear.V           (V)
import qualified Linear.V           as L
import qualified Numeric.Eigen.Raw  as R
import           Numeric.Rounded

data VectorLengthError = VectorLengthError
  deriving (Show, Exception)

data MatrixSizeError = MatrixSizeError
  deriving (Show, Exception)

fromRawVector :: KnownNat n => V.Vector a -> V n a
fromRawVector v = case L.fromVector v of
  Nothing -> throw VectorLengthError
  Just v' -> v'

fromRawMatrix :: forall j k a . (KnownNat j, KnownNat k) => MU.Matrix a -> Matrix j k a
fromRawMatrix m =
  if fromIntegral (MU.nrows m) == natVal (Proxy :: Proxy j) &&
     fromIntegral (MU.ncols m) == natVal (Proxy :: Proxy k)
  then M.matrix (m MU.!)
  else throw MatrixSizeError

luSolve
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> V j (Rounded r p)
luSolve a b = fromRawVector $ R.luSolve (M.unpackStatic a) (L.toVector b)

-- | Returns a vector of eigenvalues of 'm' and a Matrix whose columns
-- are the eigenvectors of 'm'. Here, 'm' is assumed to be symmetric.
eigensystemSymmetric
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => Matrix j j (Rounded r p)
  -> (V j (Rounded r p), Matrix j j (Rounded r p))
eigensystemSymmetric m = case R.eigensystemSymmetric (M.unpackStatic m) of
  (evals, evecs) -> (fromRawVector evals, fromRawMatrix evecs)

eigenvaluesSymmetric :: (KnownNat j, Rounding r, Precision p) => Matrix j j (Rounded r p) -> V j (Rounded r p)
eigenvaluesSymmetric = fst . eigensystemSymmetric

eigenvectorsSymmetric :: (KnownNat j, Rounding r, Precision p) => Matrix j j (Rounded r p) -> Matrix j j (Rounded r p)
eigenvectorsSymmetric = snd . eigensystemSymmetric

-- | Given a postive-definite matrix A, returns L where A = LL^T and L
-- is lower-triangular.
choleskyDecomposition
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => Matrix j j (Rounded r p)
  -> Maybe (Matrix j j (Rounded r p))
choleskyDecomposition = fmap fromRawMatrix . R.choleskyDecomposition . M.unpackStatic

ldlDecompositionPivoted
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => Matrix j j (Rounded r p)
  -> (Matrix j j (Rounded r p), V j (Rounded r p))
ldlDecompositionPivoted m = case R.ldlDecompositionPivoted (M.unpackStatic m) of
  (n, v) -> (fromRawMatrix n, fromRawVector v)
