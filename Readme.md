eigen-rounded
=============

A Haskell library for arbitrary-precision linear algebra, based on

 - [rounded](http://hackage.haskell.org/package/rounded), a Haskell library for arbitrary precision arithmetic
 - [GNU MPFR](https://www.mpfr.org/mpfr-current/mpfr.html), the Multiple Precision Floating-Point Reliable Library
 - [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page), a C++ template library for linear algebra

So far, only the following functions are supported, but it is
relatively simple to add more.

 - Linear solving with LU decomposition
 - eigenvalues/eigenvectors for symmetric real matrices
 - cholesky decomposition
