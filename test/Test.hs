{-# LANGUAGE DataKinds                #-}
{-# LANGUAGE PolyKinds                #-}
{-# LANGUAGE ScopedTypeVariables      #-}

module Numeric.Eigen.Test where

import qualified Data.Matrix              as M
import qualified Data.Vector              as V
import           Numeric.Eigen
import           Numeric.Rounded

myLuTest :: V.Vector (Rounded 'TowardZero 128)
myLuTest = 
  let
    a = M.fromLists [[2,2],[1,5]]
    b = V.fromList [1,3]
    x = luSolve a b
  in
    V.zipWith (-) b (M.getCol 1 (a * M.colVector x))

myEsystemTest :: (V.Vector (Rounded 'TowardZero 128), M.Matrix (Rounded 'TowardZero 128))
myEsystemTest = eigensystemSymmetric (M.fromLists [[1, 2, 6], [2, 3, 1], [6, 1, 7]])

myCholTest1 :: Maybe (M.Matrix (Rounded 'TowardZero 128))
myCholTest1 = 
  let m = M.fromLists [[8, 2, 6], [2, 6, 1], [6, 1, 7]]
  in case choleskyDecomposition m of
    Just l -> Just $ m - (l * M.transpose l)
    Nothing -> Nothing

myCholTest2 :: Maybe (M.Matrix (Rounded 'TowardZero 128))
myCholTest2 = choleskyDecomposition (M.fromLists [[-8, 2, 6], [2, 6, 1], [6, 1, 7]])

myLDLPivotedTest :: M.Matrix (Rounded 'TowardZero 128)
myLDLPivotedTest = 
  let m = M.fromLists [[8, 2, 6], [2, 6, 1], [6, 1, 7]]
      (ptl, d) = ldlDecompositionPivoted m
  in m - (ptl * M.diagonal 0 d * M.transpose ptl)
