#define MPFR_USE_NO_MACRO
#include "mpfr.h"
#include <Eigen/Dense>
#include <Eigen/MPRealSupport>

typedef Eigen::Matrix<mpfr::mpreal,Eigen::Dynamic,Eigen::Dynamic>  MatrixMPReal;
typedef Eigen::Matrix<mpfr::mpreal,Eigen::Dynamic,1>               VectorMPReal;

void load_vector(const mpfr_t v_arr, VectorMPReal& v) {
  for (unsigned int i = 0; i < v.rows(); ++i) {
    v(i) = mpfr::mpreal(v_arr + i);
  }
}

void save_vector(const VectorMPReal& v, mpfr_t v_arr) {
  mp_rnd_t rnd = mpfr_get_default_rounding_mode();
  for (unsigned int i = 0; i < v.rows(); ++i) {
    mpfr_set(v_arr+i, v(i).mpfr_ptr(), rnd);
  }
}

void load_matrix(const mpfr_t m_arr, MatrixMPReal& m) {
  for (unsigned int i = 0; i < m.rows(); ++i) {
    for (unsigned int j = 0; j < m.cols(); ++j) {
      m(i,j) = mpfr::mpreal(m_arr + m.cols()*i + j);
    }
  }
}

void save_matrix(const MatrixMPReal& m, mpfr_t m_arr) {
  mp_rnd_t rnd = mpfr_get_default_rounding_mode();
  for (unsigned int i = 0; i < m.rows(); ++i) {
    for (unsigned int j = 0; j < m.cols(); ++j) {
      mpfr_set(m_arr + m.cols()*i + j, m(i,j).mpfr_ptr(), rnd);
    }
  }
}

extern "C" {

  int eigen_lu_solve(int len, mpfr_t A_in, mpfr_t b_in, mpfr_t x_out, mp_rnd_t rnd, mpfr_prec_t prec)
  {
    mpfr::mpreal::set_default_rnd(rnd);
    mpfr::mpreal::set_default_prec(prec);
    MatrixMPReal A(len,len);
    load_matrix(A_in, A);
    VectorMPReal b(len);
    load_vector(b_in, b);
    VectorMPReal x = A.lu().solve(b);
    save_vector(x, x_out);
    return Eigen::Success;
  }

  int eigen_eigensystem_self_adjoint(int len, mpfr_t A_in, mpfr_t evals_out, mpfr_t evecs_out, mp_rnd_t rnd, mpfr_prec_t prec)
  {
    mpfr::mpreal::set_default_rnd(rnd);
    mpfr::mpreal::set_default_prec(prec);
    MatrixMPReal A(len,len);
    load_matrix(A_in, A);
    Eigen::SelfAdjointEigenSolver<MatrixMPReal> es(A);
    if (es.info() == Eigen::Success) {
      save_vector(es.eigenvalues(), evals_out);
      save_matrix(es.eigenvectors(), evecs_out);
      return Eigen::Success;
    } else {
      return es.info();
    }
  }

  Eigen::ComputationInfo eigen_cholesky_decomposition(int len, mpfr_t A_in, mpfr_t L_out, mp_rnd_t rnd, mpfr_prec_t prec)
  {
    mpfr::mpreal::set_default_rnd(rnd);
    mpfr::mpreal::set_default_prec(prec);
    MatrixMPReal A(len,len);
    load_matrix(A_in, A);
    // Eigen::Ref means that the decomposition operates in place
    Eigen::LLT<Eigen::Ref<MatrixMPReal> > lltOfA(A);
    if (lltOfA.info() == Eigen::Success) {
      save_matrix(lltOfA.matrixL(), L_out);
      return Eigen::Success;
    } else {
      return lltOfA.info();
    }
  }

  Eigen::ComputationInfo eigen_ldl_decomposition_pivoted(int len, mpfr_t A_in, mpfr_t PTL_out, mpfr_t D_out, mp_rnd_t rnd, mpfr_prec_t prec)
  {
    mpfr::mpreal::set_default_rnd(rnd);
    mpfr::mpreal::set_default_prec(prec);
    MatrixMPReal A(len,len);
    load_matrix(A_in, A);
    // Eigen::Ref means that the decomposition operates in place
    Eigen::LDLT<Eigen::Ref<MatrixMPReal> > ldltOfA(A);
    if (ldltOfA.info() == Eigen::Success) {
      MatrixMPReal L = ldltOfA.matrixL();
      MatrixMPReal PTL = ldltOfA.transpositionsP().transpose() * L;
      save_matrix(PTL, PTL_out);
      save_vector(ldltOfA.vectorD(), D_out);
      return Eigen::Success;
    } else {
      return ldltOfA.info();
    }
  }
}
